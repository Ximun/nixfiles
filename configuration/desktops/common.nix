{ config, pkgs, lib, ... }:

{
  # https://bugzilla.kernel.org/show_bug.cgi?id=110941
  boot.kernelParams = [ "intel_pstate=no_hwp" ];

  # Supoosedly better for the SSD.
  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.simon = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "audio" "libvirtd" "docker" ]; # Enable ‘sudo’ for the user.
    uid = 1000;
    shell = pkgs.zsh;
    #openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINOiFv7rm8ChxvFaggUHRWcgGriWxkfiIPxhUSgTeA6n ximun@aquilenet.fr" ];
  };

  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.allowBroken = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget vim emacs killall
    pulseaudio
    git
    firefox thunderbird
    xfce.thunar
    networkmanager_dmenu
    zsh-syntax-highlighting oh-my-zsh
    borgbackup
    lvm2
    gnupg pass pinentry-gtk2
    evince
    xdg-user-dirs
    nmap
    gnome3.gucharmap
    xsel
    torbrowser
    vlc
    samba
    gns3-gui
    tig
    hugo
    nixfmt
    tsocks
    atom
    elixir
    aseprite
    python39Packages.virtualenv
    gcc
    oxygen-icons5

    # Programming
    python3
    ansible # anisble-lint throws error with ansible package in venv only ...

    # hardware
    acpi

    # fonts & utils
    xorg.xfd

    # utils
    ripgrep
    pavucontrol
    htop
    bind
    inetutils
    shellcheck
    ntfs3g
    tree
    openldap
    age
    rage
    meld
    niv
    tmux
    testdisk
    unzip
    imagemagick
    openvpn
    pdftk
    qpdf
    ghostscript
    khal

    # Virtualization
    vagrant
    virt-manager
    #qemu_kvm
    #dnsmasq
    docker-compose

    # Desktop
    dino
    gimp
    libreoffice-still
    godot
    betterlockscreen
    rofi
    rofi-pass
    hunspellDicts.fr-moderne

    # Web
    yarn

    # Build
    gnumake
    #smbnetfs

    # Theming
    gnome3.adwaita-icon-theme
    adapta-gtk-theme
    xorg.xcursorthemes
    arc-theme
    lxappearance

    # Dev
    vscode
    codeblocks

    # Web
    hugo
    nodejs

    # Network
    tcpdump

    # Communication
    discord
    element-desktop

    # Gaming
    steam

  ];

  fonts.fonts = with pkgs; [
    # (nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })
    fira-code
    fira-code-symbols
    siji
    material-icons
    font-awesome
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  programs.zsh = {
    enable = true;
    promptInit = "";
    ohMyZsh = {
      enable = true;
    };
    interactiveShellInit = ''
      export ZSH=${pkgs.oh-my-zsh}/share/oh-my-zsh/
      ZSH_THEME='ys'
      source $ZSH/oh-my-zsh.sh
    '';
    shellInit = ''
      setxkbmap fr # needed fo rofi-pass fr layout
    ''; 
  };

  programs.vim.defaultEditor = true;

  programs.gnupg = {
    agent.enable = true;
  };

  # Agent SSH
  programs.ssh.startAgent = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # X Server
  services.xserver = {
    enable = true;
    windowManager.i3.enable = true;
    windowManager.i3.package = pkgs.i3-gaps;
    desktopManager.xfce.enable = true;
    layout = "fr";
    libinput.enable = true;
    libinput.tapping = true;
    displayManager.defaultSession = "none+i3";
  };

  # Home backup
  #services.borbackup.jobs = {
  #  homeSimonBackup = {
  #    paths = "/home/simon";
  #    exlude = [
        

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  networking.networkmanager.enable = true;
  networking.hosts = {
    "192.168.1.250" = [ "brunante.maison" ];
  };

  hardware.pulseaudio.enable = true;

  qt5 = {
    enable = true;
    platformTheme = "gnome";
    style = "adwaita-dark";
  };

  virtualisation = {
    libvirtd = {
      enable = true;
    };
    docker.enable = true;
  };

  services.gvfs = {
    enable = true;
    package = lib.mkForce pkgs.gnome3.gvfs;
  };

  networking.firewall.extraCommands = "iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns";

}
