# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

let
  smbaddr = "192.168.1.250";
in

{
  imports =
    [ # Include the results of the hardware scan.
      <nixos-hardware/lenovo/thinkpad/x250>
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "ciaran"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr";
  };

  

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.simon = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "audio" "libvirtd" "docker" ]; # Enable ‘sudo’ for the user.
    uid = 1000;
    shell = pkgs.zsh;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget vim killall
    pulseaudio
    git
    firefox thunderbird
    xfce.thunar
    networkmanager_dmenu
    zsh-syntax-highlighting oh-my-zsh
    borgbackup
    lvm2
    gnupg pass pinentry-gtk2
    evince
    xdg-user-dirs
    nmap
    gnome3.gucharmap
    xsel
    tor-browser-bundle-bin
    vlc
    samba
    gns3-gui
    tig
    hugo
    nixfmt
    tsocks
    atom
    elixir
    aseprite
    python39Packages.virtualenv
    gcc

    # hardware
    acpi

    # fonts & utils
    xorg.xfd

    # utils
    ripgrep
    pavucontrol
    htop
    bind
    inetutils
    shellcheck
    ntfs3g
    tree
    openldap
    age
    rage
    meld
    niv
    tmux
    testdisk
    unzip
    imagemagick

    # Virtualization
    vagrant
    virt-manager
    #qemu_kvm
    #dnsmasq
    docker-compose

    # Desktop
    dino
    element-desktop
    gimp
    libreoffice-fresh
    godot
    betterlockscreen
    rofi
    rofi-pass

    # Build
    gnumake
    #smbnetfs

    # Theming
    gnome3.adwaita-icon-theme
    adapta-gtk-theme
    xorg.xcursorthemes
    arc-theme
    lxappearance 

    # Web
    hugo
    nodejs

    # Network
    tcpdump
  ];

  fonts.fonts = with pkgs; [
    # (nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })
    fira-code
    fira-code-symbols
    siji
    material-icons
    font-awesome
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  programs.zsh = {
    enable = true;
    promptInit = "";
    #ohMyZsh = {
    #  enable = true;
    #};
    interactiveShellInit = ''
    #  export ZSH=${pkgs.oh-my-zsh}/share/oh-my-zsh/
    #  ZSH_THEME='ys'
    #  source $ZSH/oh-my-zsh.sh
    #'';
    shellInit = ''
      setxkbmap fr # needed fo rofi-pass fr layout
    ''; 
  };

  programs.vim.defaultEditor = true;

  programs.gnupg = {
    agent.enable = true;
  };

  # Agent SSH
  programs.ssh.startAgent = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # X Server
  services.xserver = {
    enable = true;
    windowManager.i3.enable = true;
    windowManager.i3.package = pkgs.i3-gaps;
    desktopManager.xfce.enable = true;
    layout = "fr";
    libinput.enable = true;
    libinput.tapping = true;
    displayManager = {
      defaultSession = "none+i3";
      lightdm.greeters.enso = {
        enable = true;
        iconTheme = {
          name = "arc";
          package = "${pkgs.arc-icon-theme}";
        };
      };
    };
  };

  # Home backup
  #services.borbackup.jobs = {
  #  homeSimonBackup = {
  #    paths = "/home/simon";
  #    exlude = [
        

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  networking.networkmanager.enable = true;
  networking.hosts = {
    "192.168.1.250" = [ "brunante.maison" ];
  };

  hardware.pulseaudio.enable = true;

  qt5 = {
    enable = true;
    platformTheme = "gnome";
    style = "adwaita-dark";
  };

  virtualisation = {
    libvirtd = {
      enable = true;
    };
    docker.enable = true;
  };

  services.gvfs = {
    enable = true;
    package = lib.mkForce pkgs.gnome3.gvfs;
  };
  networking.firewall.extraCommands = "iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns";

  fileSystems."/mnt/samba/torrent" = {
    device = "//${smbaddr}/torrent";
    fsType = "cifs";
    options = let
      # this line prevents hanging on network split
      automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
    #in ["${automount_opts},credentials=/etc/nixos/smb-secrets"];
    in ["${automount_opts}"];
  };

  fileSystems."/mnt/samba/partage" = {
    device = "//${smbaddr}/partage";
    fsType = "cifs";
    options = let
    # this line prevents hanging on network split
      automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
    #in ["${automount_opts},credentials=/etc/nixos/smb-secrets"];
    in ["${automount_opts}"];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}

